/*! [PROJECT_NAME] | Suitmedia */

((window, document, undefined) => {

    const path = {
        css: `${myPrefix}assets/css/`,
        js : `${myPrefix}assets/js/vendor/`
    }

    const assets = {
        _objectFit      : `${path.js}object-fit-images.min.js`,
        _dragdealer     : `${path.js}dragdealer.min.js`
    }

    const Site = {
        enableActiveStateMobile() {
            if ( document.addEventListener ) {
                document.addEventListener('touchstart', () => {}, true)
            }
        },

        WPViewportFix() {
            if ( '-ms-user-select' in document.documentElement.style && navigator.userAgent.match(/IEMobile/) ) {
                let style = document.createElement('style')
                let fix = document.createTextNode('@-ms-viewport{width:auto!important}')

                style.appendChild(fix)
                document.head.appendChild(style)
            }
        },

        objectFitPolyfill() {
            load(assets._objectFit).then( () => {
                objectFitImages()
            })
        },

        categoryDrag() {
            exist($('#main-handle')).then($mainHandle => {
                let handlebar   = $mainHandle.children('.handle')
                let pageSection = $('.page-category .page-section')
                let slider      = pageSection.length
                let marginslide = slider-1

                new Dragdealer('main-handle', {
                    horizontal: false,
                    vertical: true,
                    steps: slider,
                    speed: 0.2,
                    top: 100,
                    bottom: 100,
                    animationCallback: function(x, y) {
                        $('.site-category .page-category').css('margin-top', (-y * 100 * marginslide) + 'vh')
                    },
                    dragStartCallback: function(y) {
                        handlebar.removeClass('toggle-center')
                        pageSection.addClass('scaledown')
                    },
                    dragStopCallback: function(y) {
                        handlebar.addClass('toggle-center')
                        pageSection.removeClass('scaledown')
                    },
                });
            }).catch(e => {})
        }
    }

    Promise.all([
        load(assets._dragdealer)
    ]).then(() => {
        for (let fn in Site) {
            Site[fn]()
        }
        window.Site = Site
    })

    function exist(selector) {
        return new Promise((resolve, reject) => {
            let $elem = $(selector)

            if ( $elem.length ) {
                resolve($elem)
            } else {
                reject(`no element found for ${selector}`)
            }
        })
    }

    function load(url) {
        return new Promise((resolve, reject) => {
            Modernizr.load({
                load: url,
                complete: resolve
            })
        })
    }

    function loadJSON(url) {
        return new Promise((resolve, reject) => {
            fetch(url).then(res => {
                if ( res.ok ) {
                    resolve(res.json())
                } else {
                    reject('Network response not ok')
                }
            }).catch(e => {
                reject(e)
            })
        })
    }

})(window, document)
