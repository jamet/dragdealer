<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <meta name="description" content="Baze is a front-end starter template">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="format-detection" content="telephone=no">

    <meta property="og:url" content="">
    <meta property="og:title" content="">
    <meta property="og:site_name" content="">
    <meta property="og:description" content="">
    <meta property="og:image" content="">
    <meta property="og:type" content="website">
    <meta property="fb:app_id" content="">

    <meta name="twitter:card" content="">
    <meta name="twitter:site" content="">
    <meta name="twitter:description" content="">
    <meta name="twitter:title" content="">
    <meta name="twitter:image" content="">

    <link rel="apple-touch-icon" href="assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="assets/img/favicon.png">

    <title>front-end starter kit</title>

    <link rel="stylesheet" href="assets/css/main.css">
    <script src="assets/js/vendor/modernizr.min.js"></script>
</head>
<body>
    <!--[if lt IE 9]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <div class="site-dragdealer">
        <div class="dragdealer" id="main-handle">
            <div class="handle toggle-center">
                <i class="fa fa-bars fa-fw"></i>
            </div>
        </div>
    </div>
    <div class="site-category" id="main-category">
        <div class="page-category">
            <div class="page-section">
                <div class="page-section--category original">
                    <h1>Page Section 1</h1>
                </div>
            </div>
            <div class="page-section">
                <div class="page-section--category commercial">
                    <h1>Page Section 2</h1>
                </div>
            </div>
            <div class="page-section">
                <div class="page-section--category others">
                    <h1>Page Section 3</h1>
                </div>
            </div>
            <div class="page-section">
                <div class="page-section--category others">
                    <h1>Page Section 4</h1>
                </div>
            </div>
        </div>
    </div>

    <script>window.myPrefix = '';</script>
    <script src="assets/js/vendor/jquery.min.js"></script>
    <script src="https://cdn.polyfill.io/v2/polyfill.js?features=default,promise,fetch"></script>
    <script src="assets/js/main.min.js"></script>
</body>
</html>
